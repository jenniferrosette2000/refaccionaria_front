import { Component, OnInit } from '@angular/core';
import {HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.css']
})
export class SucursalComponent implements OnInit{
  sucursal: any [] =[];

  constructor (private http: HttpClient){}

  ngOnInit(){
    this.http.get('http://127.0.0.1:3900/obtener/sucursal')
    .subscribe(data => {
      this.sucursal=Object.values(data);

      //console.log(data)
      console.log('Sucursales', this.sucursal[1])
    });
   }
}
