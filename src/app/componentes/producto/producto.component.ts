import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  productos: any[] = [];
  


  constructor(private http: HttpClient){}

  ngOnInit(){
    this.http.get('http://127.0.0.1:3900/obtener/producto')
    .subscribe(data =>{
      this.productos = Object.values(data);

      //console.log(data)
      console.log('Productos',this.productos[1])
    });
  }
}
