import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {
  proveedor : any []=[];
  constructor(private http:HttpClient) { }

  ngOnInit(){
    this.http.get('http://127.0.0.1:3900/obtener/provedor')
    .subscribe(data =>{
      this.proveedor = Object.values(data);

      //console.log(data)
      console.log('Proveedores',this.proveedor[1])
    });
  }
}
