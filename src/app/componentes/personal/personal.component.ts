import { Component, OnInit } from '@angular/core';
import { HttpClient }from '@angular/common/http'

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit  {
  personal : any []=[];
  constructor( private http:HttpClient) { }

  ngOnInit(){
    this.http.get('http://127.0.0.1:3900/obtener/personal')
    .subscribe(data =>{
      this.personal = Object.values(data);

      //console.log(data)
      console.log('Personal',this.personal[1])
    });
  }

}
