import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductoComponent } from './componentes/producto/producto.component';
import { SucursalComponent } from './componentes/sucursal/sucursal.component';
import { PersonalComponent } from './componentes/personal/personal.component';
import { ProveedorComponent } from './componentes/proveedor/proveedor.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductoComponent,
    SucursalComponent,
    PersonalComponent,
    ProveedorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
